import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Vendor.css';
import { Link } from 'react-router-dom';

class HomePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            participants: [{
                participantName: "JOE L STIRSON",
                percentage: "100",
                participantTitle: "AGI eligible percent",
                participantName: "JOE L STIRSON",
                percentage: "50",
                participantEligibility: "AGI eligible percent",
                status: "Active",
                vendorRequestId: "",
                vendorNumber: "1231231313132",
                address: "125 ASLDFJLSD",
                city: "CLINTON",
                st: "KY",
                depositAccount: "5626"
            },
            {
                participantName: "JOE L STIRSON",
                percentage: "100",
                participantTitle: "AGI eligible percent",
                participantName: "JON DOE",
                percentage: "50",
                participantEligibility: "AGI eligible percent",
                status: "Active",
                vendorRequestId: "",
                vendorNumber: "1313132123123",
                address: "456 XYZ",
                city: "FAIRFAX",
                st: "KY",
                depositAccount: "4726"
            }],
            ptsIdx: -1,
            vendors: [{
                status: "Active",
                vendorNumber: "1231231313132",
                payName: "Mr Name",
                assignor: "Assign",
                address: "125 ASLDFJLSD",
                city: "CLINTON",
                st: "KY",
                zipcode: "12345",
                depositAccount: "5626"
            }],
            assignments: [
                {
                    order: "1",
                    vendorNumber: "134234234234",
                    assignor: "ASSIGNOR L",
                    assignee: "J T WORKMAN",
                    amount: 1000,
                    balance: 1100
                },
                {
                    order: "Cancelled",
                    vendorNumber: "",
                    assignor: "ASSIGNOR L",
                    assignee: "J T WORKMAN",
                    amount: 1000,
                    balance: 0
                },
                {
                    order: "Cancelled",
                    vendorNumber: "",
                    assignor: "ASSIGNOR L",
                    assignee: "J T WORKMAN",
                    amount: 1000,
                    balance: 0
                },
                {
                    order: "Cancelled",
                    vendorNumber: "",
                    assignor: "ASSIGNOR L",
                    assignee: "J T WORKMAN",
                    amount: 1000,
                    balance: 0
                },
                {
                    order: "Cancelled",
                    vendorNumber: "",
                    assignor: "ASSIGNOR L",
                    assignee: "J T WORKMAN",
                    amount: 1000,
                    balance: 0
                }
            ]
        }
    }

    render() {
        return (
            <>
                <div className="row mt-2">
                    <div className="col-8"></div>
                    <div className="col">
                        <button className="btn-top" disabled={this.state.ptsIdx < 0 }>New Vendor</button>
                        <button className="btn-top" disabled={this.state.ptsIdx < 0 }>New Assignment</button>
                        <button className="btn-top" disabled>Edit Vendor</button>
                        <button className="btn-top" disabled>Ok</button>
                        <button className="btn-top" >Cancel</button>
                    </div>
                </div>
                <div className="row title">
                    Contact Participant and Vendors
                </div>
         <div className="row participant">
                    { this.state.ptsIdx >-1 ?  this.state.participants[this.state.ptsIdx].participantName + this.state.participants[this.state.ptsIdx].percentage + '% Payment share -' +
                    this.state.participants[this.state.ptsIdx].percentage + this.state.participants[this.state.ptsIdx].participantTitle :'Select participant'}
                </div>
                <div className="row">
                    <table>

                        <tr>
                            <th>Status</th>
                            <th>Vendor Request Id</th>
                            <th>Vendor Number</th>
                            <th>Payee Name</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>ST</th>
                            <th>Deposit Account</th>
                        </tr>

                        <tbody>
                            {
                                this.state.participants.map((participant,i) => {
                                    return <tr>
                                        <td className={i==this.state.ptsIdx ? 'active-participant' : ''}><Link onClick={(e)=>this.setState(state=>({ptsIdx:i}))}>{participant.status}</Link></td>
                                        <td  className={i==this.state.ptsIdx ? 'active-participant' : ''}>{participant.vendorRequestId}</td>
                                        <td  className={i==this.state.ptsIdx ? 'active-participant' : ''}>{participant.vendorNumber}</td>
                                        <td  className={i==this.state.ptsIdx ? 'active-participant' : ''}>{participant.payName}</td>
                                        <td  className={i==this.state.ptsIdx ? 'active-participant' : ''}>{participant.address}</td>
                                        <td  className={i==this.state.ptsIdx ? 'active-participant' : ''}>{participant.city}</td>
                                        <td  className={i==this.state.ptsIdx ? 'active-participant' : ''}>{participant.st}</td>
                                        <td  className={i==this.state.ptsIdx ? 'active-participant' : ''}>{participant.depositAccount}</td>
                                    </tr>
                                }
                                )
                            }
                        </tbody>
                    </table>
                </div>
                <div className="row note">
                    <div>1. Records with zoro percent payment share are not shown</div>
                    <div>2. The participant must have an active vendor record before the obligation can be processed for the contracts</div>
                </div>
                <div className="row">
                    <div className="row title">
                        Assignments of payment and Vendors
                </div>
                    <table>
                        <thead>
                            <tr>
                                <th>Status</th>
                                <th>Vendor Number</th>
                                <th>Payee Name</th>
                                <th>Assignor</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Zipcode</th>
                                <th>Diposit Account</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.vendors.map(payee => {
                                    return <tr>
                                        <td>{payee.status}</td>
                                        <td>{payee.vendorNumber}</td>
                                        <td>{payee.payName}</td>
                                        <td>{payee.assignor}</td>
                                        <td>{payee.address}</td>
                                        <td>{payee.city}</td>
                                        <td>{payee.st}</td>
                                        <td>{payee.zipcode}</td>
                                        <td>{payee.depositAccount}</td>
                                    </tr>
                                })
                            }
                        </tbody>
                    </table>

                </div>


                <div className="row">
                    <div className="row title">
                        Assignments of Payment
                </div>
                    <table>
                        <thead>
                            <tr>
                                <th>Order</th>
                                <th>Vendor Number</th>
                                <th>Assignor</th>
                                <th>Assgnee</th>
                                <th>Amount</th>
                                <th>Blance</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.assignments.map(order => {
                                    return <tr>
                                        <td>{order.order}</td>
                                        <td>{order.vendorNumber}</td>
                                        <td>{order.assignor}</td>
                                        <td>{order.assignee}</td>
                                        <td>{order.amount}</td>
                                        <td>{order.balance}</td>
                                    </tr>
                                })
                            }
                        </tbody>
                    </table>

                </div>
            </>
        );
    }
}


export default HomePage;